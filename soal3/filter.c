#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <string.h>
#include <dirent.h>

void buatTim(int jumlah_bek, int jumlah_gelandang, int jumlah_penyerang)
{
  char namafile[150];
  sprintf(namafile, "/home/ervan/Formasi_%d-%d-%d.txt", jumlah_bek, jumlah_gelandang, jumlah_penyerang);

  pid_t child_id;
  child_id = fork();
  if (child_id == 0)
  {
    char *args[] = {"touch", namafile, NULL};
    execvp(args[0], args);
  }
  else
  {
    // memasukan kiper
    int status;
    wait(&status);
    pid_t child_id1;
    child_id1 = fork();
    if (child_id1 == 0)
    {
      int jumlah_kiper = 1;
      sprintf(namafile, "ls  Kiper/*png | sort -n -r -t _ -k4 | sed 's|kiper/ || g' | head -n %d >> /home/ervan/Formasi_%d-%d-%d.txt", jumlah_kiper, jumlah_bek, jumlah_gelandang, jumlah_penyerang);
      char *args[] = {"sh", "-c", namafile, NULL};
      execvp(args[0], args);
    }
    else
    {
      int status;
      wait(&status);
      child_id = fork(); // bek
      if (child_id == 0)
      {
        sprintf(namafile, "ls  Bek/*png | sort -n -r -t _ -k4 | sed 's|Bek/ || g' | head -n %d >> /home/ervan/Formasi_%d-%d-%d.txt", jumlah_bek, jumlah_bek, jumlah_gelandang, jumlah_penyerang);
        char *args[] = {"sh", "-c", namafile, NULL};
        execvp(args[0], args);
      }
      else
      {
        int status;
        wait(&status);
        child_id = fork();  // GElandang
        if (child_id == 0)
        {
          sprintf(namafile, "ls  Gelandang/*png | sort -n -r -t _ -k4 | sed 's|Gelandang/ || g' | head -n %d >> /home/ervan/Formasi_%d-%d-%d.txt", jumlah_gelandang, jumlah_bek, jumlah_gelandang, jumlah_penyerang);
          char *args[] = {"sh", "-c", namafile, NULL};
          execvp(args[0], args);
        }
        else
        {
          int status;
          wait(&status);
          child_id = fork(); // Penyerang
          if (child_id == 0)
          {
            sprintf(namafile, "ls  Penyerang/*png | sort -n -r -t _ -k4 | sed 's|Penyerang/ || g' | head -n %d >> /home/ervan/Formasi_%d-%d-%d.txt", jumlah_penyerang, jumlah_bek, jumlah_gelandang, jumlah_penyerang);
            char *args[] = {"sh", "-c", namafile, NULL};
            execvp(args[0], args);
          }
          else
          {
            int status;
            wait(&status);
          }
        }
      }
    }
  }
}

int main()
{
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0)
  {
    exit(EXIT_FAILURE);
  }

  if (child_id == 0)
  {
    // this is child
    char *argv[] = {"wget", "-O", "pemainbola.zip", "https://drive.google.com/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", NULL};
    execv("/usr/bin/wget", argv);
  }
  else
  {
    // this is parent
    while ((wait(&status)) > 0)
      ;
    child_id = fork();
    if (child_id == 0)
    {
      // this is child
      char *argv[] = {"unzip", "/home/ervan/sisop/Praktikum2/pemainbola.zip", NULL};
      execv("/usr/bin/unzip", argv);
    }
    else
    {
      // this is parent
      while ((wait(&status)) > 0)
        ;
      child_id = fork();
      if (child_id == 0)
      {
        // this is child
        // open directory
        remove("pemainbola.zip");
        DIR *dir = opendir("players");
        if (dir == NULL)
        {
          printf("Failed to open directory\n");
          exit(EXIT_FAILURE);
        }

        struct dirent *entry;
        char filepath[256];

        while ((entry = readdir(dir)) != NULL)
        {
          // first check if entry is a file and has the ".png"
          if (entry->d_type == DT_REG && strstr(entry->d_name, ".png"))
          {
            // second check if file name contains "manutd"
            if (strstr(entry->d_name, "ManUtd"))
            {
              continue;
            }

            // build file path
            strcpy(filepath, "players/");
            strcat(filepath, entry->d_name);
            // delete file
            if (remove(filepath) != 0)
            {
              printf("Error: Failed to delete file %s\n", entry->d_name);
            }
          }
        }

        closedir(dir);
        exit(0);
      }
      else
      {
        // this is parent
        // untuk membuat folder
        while ((wait(&status)) > 0)
          ;
        child_id = fork();
        if (child_id < 0)
        {
          exit(EXIT_FAILURE);
        }

        if (child_id == 0)
        {
          // this is child
          char *argv[] = {"mkdir", "Kiper", "Bek", "Gelandang", "Penyerang", NULL};
          execv("/usr/bin/mkdir", argv);
        }
        else
        {
          // this is parent
          while ((wait(&status)) > 0)
            ;
          DIR *dir = opendir("players");
          struct dirent *entry;
          char filepath[256]; // untuk menyimpan file path yg akan kita pindahkan
                              // looping entry
                              // hingga ketemu gelandang baru jalan ke if child
          while ((entry = readdir(dir)) != NULL)
          {
            if (strstr(entry->d_name, "Gelandang") != NULL)
            { // jika file entry mengandung gelandang maka akan masuk ke fork child

              child_id = fork();
              if (child_id < 0)
              {
                exit(EXIT_FAILURE);
              }

              if (child_id == 0)
              {
                // build file path
                strcpy(filepath, "players/");    // menambhakan file path "players/"
                strcat(filepath, entry->d_name); // menambahkan nama file kedalam  file path "/players "
                char *argv[] = {"mv", filepath, "/home/ervan/sisop/Praktikum2/Gelandang", NULL};
                execv("/usr/bin/mv", argv);
              }
            }
            else
            {
              // this is parent
              while ((wait(&status)) > 0)
                ;
              DIR *dir = opendir("players");
              if (dir == NULL)
              {
                printf("Failed to open directory\n");
                exit(EXIT_FAILURE);
              }
              struct dirent *entry;
              char filepath[256];

              while ((entry = readdir(dir)) != NULL)
              {
                if (strstr(entry->d_name, "Kiper") != NULL)
                {

                  child_id = fork();
                  if (child_id < 0)
                  {
                    exit(EXIT_FAILURE);
                  }

                  if (child_id == 0)
                  {
                    // build file path
                    strcpy(filepath, "players/");
                    strcat(filepath, entry->d_name);
                    char *argv[] = {"mv", filepath, "/home/ervan/sisop/Praktikum2/Kiper", NULL};
                    execv("/usr/bin/mv", argv);
                  }

                  else
                  {
                    // this is parent
                    while ((wait(&status)) > 0)
                      ;
                    DIR *dir = opendir("players");
                    if (dir == NULL)
                    {
                      printf("Failed to open directory\n");
                      exit(EXIT_FAILURE);
                    }
                    struct dirent *entry;
                    char filepath[256];

                    while ((entry = readdir(dir)) != NULL)
                    {
                      if (strstr(entry->d_name, "Penyerang") != NULL)
                      {

                        child_id = fork();
                        if (child_id < 0)
                        {
                          exit(EXIT_FAILURE);
                        }

                        if (child_id == 0)
                        {
                          // build file path
                          strcpy(filepath, "players/");
                          strcat(filepath, entry->d_name);
                          char *argv[] = {"mv", filepath, "/home/ervan/sisop/Praktikum2/Penyerang", NULL};
                          execv("/usr/bin/mv", argv);
                        }
                        else
                        {
                          // this is parent
                          while ((wait(&status)) > 0)
                            ;
                          DIR *dir = opendir("players");
                          if (dir == NULL)
                          {
                            printf("Failed to open directory\n");
                            exit(EXIT_FAILURE);
                          }
                          struct dirent *entry;
                          char filepath[256];

                          while ((entry = readdir(dir)) != NULL)
                          {
                            if (strstr(entry->d_name, "Bek") != NULL)
                            {

                              child_id = fork();
                              if (child_id < 0)
                              {
                                exit(EXIT_FAILURE);
                              }

                              if (child_id == 0)
                              {
                                // build file path
                                strcpy(filepath, "players/");
                                strcat(filepath, entry->d_name);
                                char *argv[] = {"mv", filepath, "/home/ervan/sisop/Praktikum2/Bek", NULL};
                                execv("/usr/bin/mv", argv);
                              }
                              else
                              {

                                while ((wait(&status)) > 0)
                                  ;
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  int bek = 4;
  int gelandang = 3;
  int penyerang = 3;
  buatTim(bek, gelandang, penyerang);
}
