# sisop-praktikum-modul-2-2023-BS-D08
> Kelompok D08

***

## Anggota Kelompok
1. I Gusti Ngurah Ervan Juli Ardana (5025211205)
2. Ghifari Maaliki Syafa Syuhada (5025211158)
3. Elmira Farah Azalia (5025211197)

---
### Soal1
Yang pertama harus dilakukan adalah mendownload sebuah file zip. Pertama, kami melakukan fork untuk melakukan wget pada child processnya dengan perintah exec untuk mendownload file zip tersebut. File zip akan didownload dengan nama bonbin.zip untuk mempermudah mengolahnya. Parent process akan menunggu child process selesai mendowndload dan programnya akan lanjut.
```
int main() {
	pid_t child_id;
	child_id = fork();

	if(child_id < 0) {
		exit(EXIT_FAILURE);
	}

	if(child_id == 0) {
		execl("/usr/bin/wget", "/usr/bin/wget", "--no-check-certificate", "https://docs.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq" , "-O", "bonbin.zip", NULL);
	} else {
	wait(NULL);
	...
```
Setelah itu kami perlu melakukan unzip pada file bonbin.zip. Maka dari itu, kami melakukan fork lagi untuk melakukan unzip dengan perintah exec pada child processnya. Kami membuat directory bernama bonbin untuk menyimpan hasil unzip dari bonbin.zip. Parent process akan menunggu child process selesai melakukan unzip dan programnya akan lanjut.
```
		...
		child_id = fork();

		if(child_id < 0) {
			exit(EXIT_FAILURE);
		}

		if(child_id == 0) {
			mkdir("bonbin", 0777);
			execl("/usr/bin/unzip", "/usr/bin/unzip", "bonbin.zip", "-d", "bonbin", NULL);
		} else {
			wait(NULL);
		...
```
Kami perlu mensortir file-file dalam bonbin.zip kedalam 3 directory yaitu HewanDarat, HewanAmphibi, dan HewanAir sesuai dengan nama akhir file tersebut. Pertama-tama kami membuat terlebih dahulu directorynya.
```
			mkdir("HewanDarat", 0777);
			mkdir("HewanAmphibi", 0777);
			mkdir("HewanAir", 0777);

```
Lalu kami akan mengolah file-filenya. Karena kami sudah menaruh isi file zip nya ke dalam directory bonbin, kami bisa menggunakan library dirent.h untuk mengolah directorynya. Disini kami perlu mendapatkan semua nama file dalam bonbin. Kami akan membaca isi directorynya, dan menyimpan list nama filenya ke dalam array filesList.
```
		...
			int file_total = 0;
			DIR *d;
			struct dirent *dir;
			d = opendir("bonbin");

			char *filesList[10];

			while((dir = readdir(d)) != NULL) {
				if(!strcmp(dir->d_name, ".") || !strcmp(dir->d_name, "..")) {
				} else {
					filesList[file_total] = (char*) malloc (strlen(dir->d_name)+1);
					strncpy(filesList[file_total],dir->d_name,strlen(dir->d_name));
					file_total++;
				}
			}

			rewinddir(d);
		...
```
Format nama dari file-file bonbin adalah [namahewan]_[habitat]. Kami perlu mengolah string nama filenya agar kami dapat mengambil nama habitat hewan tersebut. Untuk itu digunakan fungsi strtok sehinnga token yang didapat adalah string habitat hewan tersebut. Proses ini dilakukan untuk semua file. 
```
			for(int i = 0; i < file_total; i++) {
				char file_name[30];
				strcpy(file_name, filesList[i]);
				char *token = strtok(file_name, "_");
				token = strtok(NULL, "_");
				...
			}
```
Setelah mendapatkan token habitat dari file, yang perlu dilakukan adalah menentukan ke directory mana file tersebut harus dipindahkan dengan mencocokkan nama tokennya. Lalu tinggal melakukan rename dan lokasi filenya akan berpindah.
```
				if(strcmp(token,"air.jpg") == 0) {
					char dest[50] = "./HewanAir/\0";
					char src[50] = "./bonbin/\0";

					strcat(dest,filesList[i]);
					strcat(src,filesList[i]);
					rename(src, dest);
				}
				if(strcmp(token,"amphibi.jpg") == 0) {
					char dest[50] = "./HewanAmphibi/\0";
					char src[50] = "./bonbin/\0";

					strcat(dest,filesList[i]);
					strcat(src,filesList[i]);
					rename(src, dest);
				}
				if(strcmp(token,"darat.jpg") == 0) {
					char dest[50] = "./HewanDarat/\0";
					char src[50] = "./bonbin/\0";

					strcat(dest,filesList[i]);
					strcat(src,filesList[i]);
					rename(src, dest);
				}
```
Terakhir kita tinggal melakukan zip untuk ketiga directory sortir dan menghapus directory dan file lainnya yang kita buat untuk menghemat memori.
```
			char *zip_list[3] = {"HewanDarat", "HewanAir", "HewanAmphibi"};
			char *zip_name[3] = {"HewanDarat.zip", "HewanAir.zip", "HewanAmphibi.zip"};
			for(int i = 0; i < 3; i++) {
				child_id = fork();

				if(child_id < 0) {
					exit(EXIT_FAILURE);
				}

				if(child_id == 0) {
					execl("/usr/bin/zip", "/usr/bin/zip", "-r", zip_name[i], zip_list[i], NULL);
				}
			}
			wait(NULL);
			execl("/usr/bin/rm", "/usr/bin/rm", "-r", "bonbin", "HewanDarat", "HewanAir", "HewanAmphibi", "bonbin.zip", NULL);
```
### Soal2
Kami perlu membuat sebuah program yang berjalan secara daemon yang akan mendownload file ke sebuah directory dengan interval 5 detik per filenya dan membuat sebuah directory dengan interval 30 detik per directory. Perlu diingat bahwa setiap directory membutuhkan 15 file, dan process mendownload pada setiap directory dimulai setelah directory dibuat. Hal ini memerlukan beberapa process untuk bekerja mendownload file secara bersamaan.

Pertama kami harus membuat sebuah directory sesuai dengan waktu pada saat itu. Disini kami menggunakan library time.h untuk mengambil nilai waktu. Lalu kami rangkai nilai waktu tersebut sesuai dengan format penamaan directory di soal dan setelah itu membuat directorynya.
```
	while (1) {
		time_t T = time(NULL);
		struct tm date_T = *localtime(&T);
		char dir_name[50];
		int st;

		snprintf(dir_name, sizeof(dir_name), "%d-%d-%d_%d:%d:%d",
			date_T.tm_year + 1900, date_T.tm_mon, date_T.tm_mday,
			date_T.tm_hour, date_T.tm_min, date_T.tm_sec);
		mkdir(dir_name,0777);
```
Setelah itu kami perlu mendownload dari https://picsum.photos. Website ini memiliki fitur unik dimana apabila kita menambahkan sebuah bilangan di akhir urlnya maka website akan mengembalikan gambar random dengan ukuran sesuai bilangan tersebut. Contoh https://picsum.photos/200 akan mengembalikan gambar berukuran 200x200 pixel. Soal menyuruh kami untuk mendownload gambar dengan ukuran sesuai dengan t%1000+50 (t adalah jumlah detik semenjak epoch). Untuk itu, kami akan melakukan fork agar child process dapat melakukan perintah wget dengan fungsi exec. Karena yang dibutuhkan adalah 15 file maka kami menggunakan for loop untuk mengulangi proses mendownloadnya. Yang akan terjadi adalah process ini akan melakukan fork sebanyak 15 kali dan child process dari process ini yang akan melakukan download. Download berjalan setiap 5 detik, maka setelah child processnya mendownload, parent processnya akan melakaukan sleep(5).
```
		pid_t child_id;
		child_id = fork();

		if (child_id < 0) {
			exit(EXIT_FAILURE);
		}

		if (child_id == 0) {
			for(int i = 0; i < 15; i++) {
				child_id = fork();
				if(child_id < 0) {
					exit(EXIT_FAILURE);
				}

				if(child_id == 0) {
					time_t t = time(NULL);
					struct tm date_t = *localtime(&t);
					char file_name[100];
					
					snprintf(file_name, sizeof(file_name), "%s/%d-%d-%d_%d:%d:%d",
						dir_name, date_t.tm_year + 1900, date_t.tm_mon, date_t.tm_mday,
						date_t.tm_hour, date_t.tm_min, date_t.tm_sec);

					char uri_name[100];
					snprintf(uri_name, sizeof(uri_name),
						"https://picsum.photos/%d", ((int) t % 1000) + 50);

					char dest[100];
					strcpy(dest,path);
					strcat(dest,file_name);
					
					execl("/usr/bin/wget", "/usr/bin/wget", "-bqc",  uri_name, "-O", file_name, "&", NULL);
				} else {
					sleep(5);
				}
			}
```
Setelah terdownload 15 file, directory akan dizip dan dihapus.
```
			char dest[100];
			strcpy(dest,dir_name);
			strcat(dest, ".zip");

			pid_t child_id1;
			child_id1 = fork();

			if(child_id1 < 0) {
				exit(EXIT_FAILURE);
			}

			if(child_id1 == 0) {
				execl("/usr/bin/zip", "/usr/bin/zip", "-r", dest, dir_name,  NULL);
			} else {
				int st1;
				waitpid(child_id1, &st1, 0);
				execl("/usr/bin/rm", "/usr/bin/rm", "-r",  dir_name, NULL);
			}
```
Setiap directory memiliki jeda 30 detik, maka hal terakhir yang harus kita lakukan adalah melakukan sleep(30)
```
		} else {
			sleep(30);
		}
```
Terakhir, kami perlu membuat MODE A dan MODE B. MODE A ketika dijalankan akan menghentikan seluruh process (KILLALL) sedangkan MODE B ketika dijalankan akan menghentikan daemon processnya saja sehingga memberi kesempatan untuk process lain menyelesaikan tugasnya (PKILL). MODE A dan MODE B ini diakses dengan argumen -a dan -b pada program kami. Di dalam program kami menambahkan kode seperti berikut
```
	if(strcmp(argv[1], "-a") == 0) {
		execl("/usr/bin/killall", "/usr/bin/killall", "lukisan", NULL);
	} else if (strcmp(argv[1], "-b") == 0) {
		execl("/usr/bin/pkill", "/usr/bin/pkill", "-o", "lukisan", NULL );
	}
``` 
### Soal3
#### - Pertanyaan:
Ten Hag adalah seorang pelatih Ajax di Liga Belanda. Suatu hari, Ten Hag mendapatkan tawaran untuk menjadi manajer Manchester United. Karena Ten Hag masih mempertimbangkan tawaran tersebut, ia ingin mengenal para pemain yang akan dilatih kedepannya. Dikarenakan Ten Hag hanya mendapatkan url atau link database mentah para pemain bola, maka ia perlu melakukan klasifikasi pemain Manchester United. Bantulah Ten Hag untuk mengenal para pemain Manchester United tersebut hanya dengan 1 Program C bernama “filter.c”
1. Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.
2. Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.  
3. Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.
4. Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/

#### - Solusi:
1. Pada point a, pertama tama kita harus melakukan fork pada parent proses. setelah melakukan fork pada parent proses kita melakukan proses download pada child proses nya dengan perintah execv dan menggunakan wget. Sehingga nanti hasil download akan bernama "pemainbola.zip"
```
pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0)
  {
    exit(EXIT_FAILURE);
  }

  if (child_id == 0)
  {
    char *argv[] = {"wget", "-O", "pemainbola.zip", "https://drive.google.com/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", NULL};
    execv("/usr/bin/wget", argv);
  }
  else
  {
    while ((wait(&status)) > 0)
```
Setelah itu Parent proses pada fork yg pertama akan dilakukan fork lagi sehingga menghasilkan child proses baru. pada child proses nanti akan dilakukan execv dengan melakukan unzip. dan di parent proses kita menunggu child proses sebelumnya kelar terlebih dahulu kemudian melanjutkan dengan melakukan remove pada pemainbola.zip
```
else
  {
    while ((wait(&status)) > 0)
      ;
    child_id = fork();
    if (child_id == 0)
    {
      char *argv[] = {"unzip", "/home/ervan/sisop/Praktikum2/pemainbola.zip", NULL};
      execv("/usr/bin/unzip", argv);
    }
    else
    {
      while ((wait(&status)) > 0)
        ;
	child_id = fork();
      if (child_id == 0)
      {
        // this is child
        // open directory
        remove("pemainbola.zip");
```
2. pada point B, program akan melakukan fork lagi pada parent proses sebelumnya untuk menghasilkan child proses baru. pada child proses yang baru program akan membuka folder players dengan perintah opendir(). Jika direktori tidak berhasil dibuka, maka program akan mencetak pesan kesalahan dan keluar dari program. 
Program akan membaca setiap file di dalam direktori "players" menggunakan fungsi readdir(). program akan memeriksa setiap file untuk memastikan bahwa file tersebut adalah sebuah file ".png" menggunakan variabel entry->d_type dan fungsi strstr(). Selain itu, proses anak akan memeriksa setiap nama file untuk memastikan bahwa file tersebut tidak mengandung kata "ManUtd" menggunakan fungsi strstr(). Jika file memenuhi kedua kondisi tersebut, program akan menghapus file tersebut menggunakan fungsi remove(). Jika file tidak dapat dihapus, maka proses anak akan mencetak pesan kesalahan.
```
child_id = fork();
      if (child_id == 0)
      {
        // this is child
        // open directory
        remove("pemainbola.zip");
        DIR *dir = opendir("players");
        if (dir == NULL)
        {
          printf("Failed to open directory\n");
          exit(EXIT_FAILURE);
        }

        struct dirent *entry;
        char filepath[256];

        while ((entry = readdir(dir)) != NULL)
        {
          // first check if entry is a file and has the ".png"
          if (entry->d_type == DT_REG && strstr(entry->d_name, ".png"))
          {
            // second check if file name contains "manutd"
            if (strstr(entry->d_name, "ManUtd"))
            {
              continue;
            }

            // build file path
            strcpy(filepath, "players/");
            strcat(filepath, entry->d_name);
            // delete file
            if (remove(filepath) != 0)
            {
              printf("Error: Failed to delete file %s\n", entry->d_name);
            }
          }
        }
```
3. Pada point c, program akan melakukan fork lagi pada parent proses sebelumnya. pada child proses nya kita melakukan perintah execv dengan melakukan mkdir untuk membuat direktori baru untuk memisahkan pemain berdasarkan posisi mereka.
```
else
      {
        // this is parent
        // untuk membuat folder
        while ((wait(&status)) > 0)
          ;
        child_id = fork();
        if (child_id < 0)
        {
          exit(EXIT_FAILURE);
        }

        if (child_id == 0)
        {
          // this is child
          char *argv[] = {"mkdir", "Kiper", "Bek", "Gelandang", "Penyerang", NULL};
          execv("/usr/bin/mkdir", argv);
        }
        else
        {
          // this is parent
          while ((wait(&status)) > 0)
            ;
```
setelah berhasil membuat 4 buah direktori berdasarkan posisi pemain. program akan melakukan fork lagi pada parent proses sebelumnya. Kemudian program membuka direktori "players" menggunakan fungsi opendir(). Jika direktori gagal dibuka, maka program mencetak pesan kesalahan dan keluar dari program menggunakan exit(EXIT_FAILURE). Kemudian program melakukan loop pada setiap file dalam direktori menggunakan fungsi readdir().Pada setiap file yang ditemukan, program melakukan pengecekan. program memeriksa apakah nama file tersebut mengandung string "Gelandang" . Jika file memenuhi kriteria "Gelandang", maka program akan melakukan proses fork() lagi dan menambahkan string player/ kedalam file path dan menyalin nama nama file yg sesuai kriteria kedalam file path kemudian program akan memindahkan file yg sesuai dengan kriteria tersebut ke direktori "Gelandang" menggunakan fungsi execv() dengan argumen mv. begitupun dengan posisi selanjutnya. program akan membuka file player lagi, kemudia melakukan pengecekan apakah terdapat kata kunci "kiper" jika ada maka akan dipindahkan ke folder kiper seperti proses yg sama dengan proses sebelumnya. Proses ini akan berlanjut hingga posisi akhir dan folder player akan menjadi kosong karena isinya telah dipindahkan ke masing masing folder posisi. 
``` 
else
        {
          // this is parent
          while ((wait(&status)) > 0)
            ;
          DIR *dir = opendir("players");
          struct dirent *entry;
          char filepath[256]; // untuk menyimpan file path yg akan kita pindahkan
                              // looping entry
                              // hingga ketemu gelandang baru jalan ke if child
          while ((entry = readdir(dir)) != NULL)
          {
            if (strstr(entry->d_name, "Gelandang") != NULL)
            { // jika file entry mengandung gelandang maka akan masuk ke fork child

              child_id = fork();
              if (child_id < 0)
              {
                exit(EXIT_FAILURE);
              }

              if (child_id == 0)
              {
                // build file path
                strcpy(filepath, "players/");    // menambhakan file path "players/"
                strcat(filepath, entry->d_name); // menambahkan nama file kedalam  file path "/players "
                char *argv[] = {"mv", filepath, "/home/ervan/sisop/Praktikum2/Gelandang", NULL};
                execv("/usr/bin/mv", argv);
              }
            }
            else
            {
              // this is parent
              while ((wait(&status)) > 0)
                ;
              DIR *dir = opendir("players");
              if (dir == NULL)
              {
                printf("Failed to open directory\n");
                exit(EXIT_FAILURE);
              }
              struct dirent *entry;
              char filepath[256];

              while ((entry = readdir(dir)) != NULL)
              {
                if (strstr(entry->d_name, "Kiper") != NULL)
                {

                  child_id = fork();
                  if (child_id < 0)
                  {
                    exit(EXIT_FAILURE);
                  }

                  if (child_id == 0)
                  {
                    // build file path
                    strcpy(filepath, "players/");
                    strcat(filepath, entry->d_name);
                    char *argv[] = {"mv", filepath, "/home/ervan/sisop/Praktikum2/Kiper", NULL};
                    execv("/usr/bin/mv", argv);
                  }

                  else
                  {
                    // this is parent
                    while ((wait(&status)) > 0)
                      ;
                    DIR *dir = opendir("players");
                    if (dir == NULL)
                    {
                      printf("Failed to open directory\n");
                      exit(EXIT_FAILURE);
                    }
                    struct dirent *entry;
                    char filepath[256];

                    while ((entry = readdir(dir)) != NULL)
                    {
                      if (strstr(entry->d_name, "Penyerang") != NULL)
                      {

                        child_id = fork();
                        if (child_id < 0)
                        {
                          exit(EXIT_FAILURE);
                        }

                        if (child_id == 0)
                        {
                          // build file path
                          strcpy(filepath, "players/");
                          strcat(filepath, entry->d_name);
                          char *argv[] = {"mv", filepath, "/home/ervan/sisop/Praktikum2/Penyerang", NULL};
                          execv("/usr/bin/mv", argv);
                        }
                        else
                        {
                          // this is parent
                          while ((wait(&status)) > 0)
                            ;
                          DIR *dir = opendir("players");
                          if (dir == NULL)
                          {
                            printf("Failed to open directory\n");
                            exit(EXIT_FAILURE);
                          }
                          struct dirent *entry;
                          char filepath[256];

                          while ((entry = readdir(dir)) != NULL)
                          {
                            if (strstr(entry->d_name, "Bek") != NULL)
                            {

                              child_id = fork();
                              if (child_id < 0)
                              {
                                exit(EXIT_FAILURE);
                              }

                              if (child_id == 0)
                              {
                                // build file path
                                strcpy(filepath, "players/");
                                strcat(filepath, entry->d_name);
                                char *argv[] = {"mv", filepath, "/home/ervan/sisop/Praktikum2/Bek", NULL};
                                execv("/usr/bin/mv", argv);
                              }
                              else
                              {

                                while ((wait(&status)) > 0)
                                  ;
                              }
```
4. pada point d, kami diminta untuk membuat sebuah fungsi untuk membuat kesebelesan terbaik dari masing masing posisi tersebut. pertama tama kami melakukan fork terlebih dahulu pada parent prosesnya. kemudian pada child proses nua kita melakukan proses execvp dengan membuat file.txt sesuai dengan kriteria yg diminta. setelah itu pada parent prosesnya kita melakukan fork lagi, dan pada child proses yang baru kita akan memasukan nama kiper kepada file.txt dengan pertama melakukan filter terlebih dahulu berdasarkan number dari nama file yg ada di folder kiper. setelah dilakukan filter maka akan dilakukan pemindahan menuju file.txt sesuai jumlah yg di inginkan. karena kiper hanya wajib memindahkan 1 maka kami sudah mendklarasikan jumlah_kiper = 1 terlebih dahulu. Sehingga nanti hanya 1 pemain yg akan dipindahkan menuju file .txt. begitu pula dengan posisi lain akan dijalankan dengan proses yg sama hanya dengan jumlah yg berbeda.
``` 
void buatTim(int jumlah_bek, int jumlah_gelandang, int jumlah_penyerang)
{
  char namafile[150];
  sprintf(namafile, "/home/ervan/Formasi_%d-%d-%d.txt", jumlah_bek, jumlah_gelandang, jumlah_penyerang);

  pid_t child_id;
  child_id = fork();
  if (child_id == 0)
  {
    char *args[] = {"touch", namafile, NULL};
    execvp(args[0], args);
  }
  else
  {
    // memasukan kiper
    int status;
    wait(&status);
    pid_t child_id1;
    child_id1 = fork();
    if (child_id1 == 0)
    {
      int jumlah_kiper = 1;
      sprintf(namafile, "ls  Kiper/*png | sort -n -r -t _ -k4 | sed 's|kiper/ || g' | head -n %d >> /home/ervan/Formasi_%d-%d-%d.txt", jumlah_kiper, jumlah_bek, jumlah_gelandang, jumlah_penyerang);
      char *args[] = {"sh", "-c", namafile, NULL};
      execvp(args[0], args);
    }
    else
    {
      int status;
      wait(&status);
      child_id = fork();
      if (child_id == 0)
      {
        sprintf(namafile, "ls  Bek/*png | sort -n -r -t _ -k4 | sed 's|Bek/ || g' | head -n %d >> /home/ervan/Formasi_%d-%d-%d.txt", jumlah_bek, jumlah_bek, jumlah_gelandang, jumlah_penyerang);
        char *args[] = {"sh", "-c", namafile, NULL};
        execvp(args[0], args);
      }
      else
      {
        int status;
        wait(&status);
        child_id = fork();
        if (child_id == 0)
        {
          sprintf(namafile, "ls  Gelandang/*png | sort -n -r -t _ -k4 | sed 's|Gelandang/ || g' | head -n %d >> /home/ervan/Formasi_%d-%d-%d.txt", jumlah_gelandang, jumlah_bek, jumlah_gelandang, jumlah_penyerang);
          char *args[] = {"sh", "-c", namafile, NULL};
          execvp(args[0], args);
        }
        else
        {
          int status;
          wait(&status);
          child_id = fork();
          if (child_id == 0)
          {
            sprintf(namafile, "ls  Penyerang/*png | sort -n -r -t _ -k4 | sed 's|Penyerang/ || g' | head -n %d >> /home/ervan/Formasi_%d-%d-%d.txt", jumlah_penyerang, jumlah_bek, jumlah_gelandang, jumlah_penyerang);
            char *args[] = {"sh", "-c", namafile, NULL};
            execvp(args[0], args);
          }
          else
          {
            int status;
            wait(&status);
          }
        }
      }
    }
  }
}

```
setelah fungsi buatTim() selesai dibuat maka, fungsi tersebut akan dipanggil di akhir program

```
int bek = 4;
  int gelandang = 3;
  int penyerang = 3;
  buatTim(bek, gelandang, penyerang);
```


### Soal4
Pertama kami menggunakan fungsi validate_cron_arg yang menerima tiga argumen; argumen yang akan divalidasi, nilai minimal, dan nilai maksimal untuk menentukan apakah argumen valid atau tidak.

```
int validate_cron_arg(char* arg, int min_val, int max_val) {
    if (strcmp(arg, "*") == 0) {
        return 1;
    }

    int value = atoi(arg);
    if (value < min_val || value > max_val) {
        return 0;
    }
    return 1;
}
```
Pada fungsi main memeriksa argumen yang diterima, jika argumen ≠ 5 maka mencetak pesan error. Lalu fungsi memeriksa argumen jam, menit, detik pada masing-masing argumen. Jika salah satu argumen tidak valid, maka mencetak error, jika valid maka fungsi menyimpan argumen pada variabel masing-masing.

```
int main(int argc, char **argv) {
    if (argc != 5) {
        printf("Error: Argumen tidak sesuai\n");
        return 1;
    }

    if (!validate_cron_arg(argv[1], 0, 23) || !validate_cron_arg(argv[2], 0, 59) || !validate_cron_arg(argv[3], 0, 59)) {
        printf("Error: Argumen tidak sesuai\n");
        return 1;
    }

    char *cron_hour = argv[1];
    char *cron_minute = argv[2];
    char *cron_second = argv[3];
    char *cron_file = argv[4];
```
Lalu kami melakukan proses fork untuk membuat proses baru. Jika proses fork berhasil, maka proses anak berhasil dibuat, agar dapat menjalankan proses daemon, proses anak melakukan untuk membuat session baru serta pemilik session.umask(0) dilakukan untuk proses anak mendapatkan akses full terhadap file yang dibuat oleh daemon. Kemudian perintah close agar tidak lagi menerima input/output dari terminal tersebut. Sebaliknya, proses anak akan menggunakan file yang telah dibuka sebelumnya untuk melakukan input/output. Dengan proses ini program dapat berjalan pada background tanpa membutuhkan interaksi langsung dengan pengguna.

```
    pid_t pid = fork();

    if (pid == -1) {
        printf("Error: Fork gagal\n");
        return 1;
    } else if (pid == 0) {
        if (setsid() == -1) {
            printf("Error: Session id gagal\n");
            return 1;
        }
        umask(0);

        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);
```
Program kemudian membandingkan jadwal cron dengan waktu saat ini dengan menggunakan fungsi 'strcmp' dan 'atoi'. Jika waktu saat ini sesuai dengan jadwal cron yang telah ditentukan, program akan menjalankan perintah shell yang didefinisikan dalam variabel cmd menggunakan fungsi execl. Loop akan terus berjalan selama program belum dihentikan. Jika program selesai dieksekusi, program akan mengembalikan nilai 0.

```
        while (1) {
   		  time_t now = time(NULL);
   		  struct tm *tm_now = localtime(&now);

 		    if (strcmp(cron_hour, "*") == 0 || atoi(cron_hour) == tm_now->tm_hour) {
        if (strcmp(cron_minute, "*") == 0 || atoi(cron_minute) == tm_now->tm_min) {
        if (strcmp(cron_second, "*") == 0 || atoi(cron_second) == tm_now->tm_sec) {

            char cmd[100];
            sprintf(cmd, "%s %s %s * %s", cron_hour, cron_minute, cron_second, cron_file);
            execl("/bin/sh", "sh", "-c", cmd, NULL);
           }
        }
     }
     sleep (1);
	  }
   }
   return (0);
}
```
Untuk mencompile kode dalam bahasa c 
```
gcc -o mainan mainan.c
```
Contoh argumen untuk menjalankan program
```
./mainan 13 8 20 ./file.sh
```
Untuk melihat apakah proses berhasil berjalan
```
ps -aux | grep "mainan"
```
