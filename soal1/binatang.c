#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <dirent.h>

int main() {
	pid_t child_id;
	child_id = fork();

	if(child_id < 0) {
		exit(EXIT_FAILURE);
	}

	if(child_id == 0) {
		execl("/usr/bin/wget", "/usr/bin/wget", "--no-check-certificate", "https://docs.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq" , "-O", "bonbin.zip", NULL);
	} else {
		wait(NULL);
		child_id = fork();

		if(child_id < 0) {
			exit(EXIT_FAILURE);
		}

		if(child_id == 0) {
			mkdir("bonbin", 0777);
			execl("/usr/bin/unzip", "/usr/bin/unzip", "bonbin.zip", "-d", "bonbin", NULL);
		} else {
			wait(NULL);

			mkdir("HewanDarat", 0777);
			mkdir("HewanAmphibi", 0777);
			mkdir("HewanAir", 0777);

			int file_total = 0;
			DIR *d;
			struct dirent *dir;
			d = opendir("bonbin");

			char *filesList[10];

			while((dir = readdir(d)) != NULL) {
				if(!strcmp(dir->d_name, ".") || !strcmp(dir->d_name, "..")) {
				} else {
					filesList[file_total] = (char*) malloc (strlen(dir->d_name)+1);
					strncpy(filesList[file_total],dir->d_name,strlen(dir->d_name));
					file_total++;
				}
			}

			rewinddir(d);

			for(int i = 0; i < file_total; i++) {
				char file_name[30];
				strcpy(file_name, filesList[i]);
				char *token = strtok(file_name, "_");
				token = strtok(NULL, "_");
				if(strcmp(token,"air.jpg") == 0) {
					char dest[50] = "./HewanAir/\0";
					char src[50] = "./bonbin/\0";

					strcat(dest,filesList[i]);
					strcat(src,filesList[i]);
					rename(src, dest);
				}
				if(strcmp(token,"amphibi.jpg") == 0) {
					char dest[50] = "./HewanAmphibi/\0";
					char src[50] = "./bonbin/\0";

					strcat(dest,filesList[i]);
					strcat(src,filesList[i]);
					rename(src, dest);
				}
				if(strcmp(token,"darat.jpg") == 0) {
					char dest[50] = "./HewanDarat/\0";
					char src[50] = "./bonbin/\0";

					strcat(dest,filesList[i]);
					strcat(src,filesList[i]);
					rename(src, dest);
				}
			}
			
			char *zip_list[3] = {"HewanDarat", "HewanAir", "HewanAmphibi"};
			char *zip_name[3] = {"HewanDarat.zip", "HewanAir.zip", "HewanAmphibi.zip"};
			for(int i = 0; i < 3; i++) {
				child_id = fork();

				if(child_id < 0) {
					exit(EXIT_FAILURE);
				}

				if(child_id == 0) {
					execl("/usr/bin/zip", "/usr/bin/zip", "-r", zip_name[i], zip_list[i], NULL);
				}
			}
			wait(NULL);
			execl("/usr/bin/rm", "/usr/bin/rm", "-r", "bonbin", "HewanDarat", "HewanAir", "HewanAmphibi", "bonbin.zip", NULL);
		}
	}
}
