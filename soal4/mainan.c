#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <ctype.h>
#include <time.h>

int validate_cron_arg(char* arg, int min_val, int max_val) {
    if (strcmp(arg, "*") == 0) {
        return 1;
    }

    int value = atoi(arg);
    if (value < min_val || value > max_val) {
        return 0;
    }
    return 1;
}

int main(int argc, char **argv) {
    if (argc != 5) {
        printf("Error: Argumen tidak sesuai\n");
        return 1;
    }

    if (!validate_cron_arg(argv[1], 0, 23) || !validate_cron_arg(argv[2], 0, 59) || !validate_cron_arg(argv[3], 0, 59)) {
        printf("Error: Argumen tidak sesuai\n");
        return 1;
    }

    char *cron_hour = argv[1];
    char *cron_minute = argv[2];
    char *cron_second = argv[3];
    char *cron_file = argv[4];

    pid_t pid = fork();

    if (pid == -1) {
        printf("Error: Fork gagal\n");
        return 1;
    } else if (pid == 0) {
        if (setsid() == -1) {
            printf("Error: Session id gagal\n");
            return 1;
        }
        umask(0);

        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);

        while (1) {
   		time_t now = time(NULL);
   		struct tm *tm_now = localtime(&now);

 		if (strcmp(cron_hour, "*") == 0 || atoi(cron_hour) == tm_now->tm_hour) {
        if (strcmp(cron_minute, "*") == 0 || atoi(cron_minute) == tm_now->tm_min) {
        if (strcmp(cron_second, "*") == 0 || atoi(cron_second) == tm_now->tm_sec) {

            char cmd[100];
            sprintf(cmd, "%s %s %s * %s", cron_hour, cron_minute, cron_second, cron_file);
            execl("/bin/sh", "sh", "-c", cmd, NULL);
           }
        }
     }
     sleep (1);
	 }
   }
   return (0);
}
