#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <time.h>
#include <signal.h>

int main(int argc, char **argv) {
	pid_t pid, sid;
	pid = fork();


	if (pid < 0) {
		exit(EXIT_FAILURE);
	}

	if (pid > 0) {
		exit(EXIT_SUCCESS);
	}

	umask(0);

	sid = setsid();
	if (sid < 0) {
		exit(EXIT_FAILURE);
	}

	char path[] = "/home/gmaaliki/Pictures";

	if ((chdir(path)) < 0) {
		exit(EXIT_FAILURE);
	}

	if(strcmp(argv[1], "-a") == 0) {
		execl("/usr/bin/killall", "/usr/bin/killall", "lukisan", NULL);
	} else if (strcmp(argv[1], "-b") == 0) {
		execl("/usr/bin/pkill", "/usr/bin/pkill", "-o", "lukisan", NULL );
	}

	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);

	while (1) {
		time_t T = time(NULL);
		struct tm date_T = *localtime(&T);
		char dir_name[50];
		int st;

		snprintf(dir_name, sizeof(dir_name), "%d-%d-%d_%d:%d:%d",
			date_T.tm_year + 1900, date_T.tm_mon, date_T.tm_mday,
			date_T.tm_hour, date_T.tm_min, date_T.tm_sec);
		mkdir(dir_name,0777);

		pid_t child_id;
		child_id = fork();

		if (child_id < 0) {
			exit(EXIT_FAILURE);
		}

		if (child_id == 0) {
			for(int i = 0; i < 15; i++) {
				child_id = fork();
				if(child_id < 0) {
					exit(EXIT_FAILURE);
				}

				if(child_id == 0) {
					time_t t = time(NULL);
					struct tm date_t = *localtime(&t);
					char file_name[100];
					
					snprintf(file_name, sizeof(file_name), "%s/%d-%d-%d_%d:%d:%d",
						dir_name, date_t.tm_year + 1900, date_t.tm_mon, date_t.tm_mday,
						date_t.tm_hour, date_t.tm_min, date_t.tm_sec);

					char uri_name[100];
					snprintf(uri_name, sizeof(uri_name),
						"https://picsum.photos/%d", ((int) t % 1000) + 50);

					char dest[100];
					strcpy(dest,path);
					strcat(dest,file_name);
					
					execl("/usr/bin/wget", "/usr/bin/wget", "-bqc",  uri_name, "-O", file_name, "&", NULL);
				} else {
					sleep(5);
				}
			}

			char dest[100];
			strcpy(dest,dir_name);
			strcat(dest, ".zip");

			pid_t child_id1;
			child_id1 = fork();

			if(child_id1 < 0) {
				exit(EXIT_FAILURE);
			}

			if(child_id1 == 0) {
				execl("/usr/bin/zip", "/usr/bin/zip", "-r", dest, dir_name,  NULL);
			} else {
				int st1;
				waitpid(child_id1, &st1, 0);
				execl("/usr/bin/rm", "/usr/bin/rm", "-r",  dir_name, NULL);
			}
		} else {
			sleep(30);
		}
	}
}
